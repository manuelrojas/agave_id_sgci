# agave_id_sgci

### Clone
```
$ git clone <repo>
$ cd agave_id_sgci
```

### Configure settings

Settings should be already set for your testing environemnt at `agave_id_sgci/agave_id/local_settings_example.py`:

```
BASE_URL='192.168.99.100'
BASE_PORT=8000
EMAIL_HOST = mail_server            # localhost
EMAIL_PORT = mail_server_port       # 1025
EMAIL_BASE_URL = 'http://' + BASE_URL + ':' + str(BASE_PORT)
CONTINUE_URL='https://catalog.sciencegateways.org'   # your continue button url here
```

### Build
```
$ docker-compose build
$ docker-compose up -d
```
If succesful. You should see the `agaveidsgci_ldap_1` and `agaveidsgci_agaveid_1` containers:
```
$ docker ps
        Name                       Command               State           Ports
---------------------------------------------------------------------------------------
agaveidsgci_agaveid_1   /bin/sh -c /usr/sbin/apach ...   Up      0.0.0.0:8000->80/tcp
agaveidsgci_ldap_1      /sbin/my_init                    Up      0.0.0.0:10389->389/tcp
```

### Test
Create ldap's OU:
```
$ curl -X POST --data ou=tenant1 192.168.99.100:8000/ous | python -m json.tool
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   120    0   110  100    10    233     21 --:--:-- --:--:-- --:--:--   233
{
    "message": "OU created successfully.",
    "result": {},
    "status": "success",
    "version": "2.0.0-SNAPSHOT-rc3fad"
}
```
Create example user:
```
$ curl -X POST --data "username=jdoe&password=abcde&email=jdoe@test.com" 192.168.99.100:8000/profiles/v2  | python -m json.tool
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   330    0   282  100    48   4282    728 --:--:-- --:--:-- --:--:--  4338
{
    "message": "User created successfully.",
    "result": {
        "email": "jdoe@test.com",
        "first_name": "",
        "full_name": "jdoe",
        "last_name": "jdoe",
        "mobile_phone": "",
        "phone": "",
        "status": "Active",
        "uid": null,
        "username": "jdoe"
    },
    "status": "success",
    "version": "2.0.0-SNAPSHOT-rc3fad"
}
```
If both of these requests are working properly. Proceed to create account through the app.
First your must stand an SMTP server inside your `agaveidsgci_agaveid_1` container for the account verification:
```
$ docker exec -it agaveidsgci_agaveid_1 bash
# python -m smtpd -n -c DebuggingServer localhost:1025
```
You should be set to test now at the browser by going to: `192.168.99.100:8000/create_account`
On form submit your should see something like on your debugging server:
```
---------- MESSAGE FOLLOWS ----------
MIME-Version: 1.0
Content-Type: text/plain; charset="utf-8"
Content-Transfer-Encoding: 7bit
Subject: New Agave Account Requested
From: do-not-reply@agaveapi.io
To: mrojas@tacc.utexas.edu
Date: Fri, 03 Feb 2017 15:25:36 -0000
Message-ID: <20170203152536.8.62220@afbb44ecb783>
X-Peer: 127.0.0.1

Dear Manuel Rojas,
Welcome to Science Gateways, we are looking forward to working with you!
In order to activate your account click on the following link to confirm your email address:

http://192.168.99.100:8000/user/validate?username=mrojas&nonce=381758974

Thanks,
Science Gateways Admin
------------ END MESSAGE ------------
```
Finish the account registration by going to the URL provided:
```
http://192.168.99.100:8000/user/validate?username=mrojas&nonce=381758974
```
